/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#define VERSION "0.2015.06.08.000"

#include <iostream>
#include <libintl.h>
#include <getopt.h>

//-----------------INI MACROS-------------------------------------------
#define GETTEXT_DOMAIN	"RealTimeBattle_reloaded"
#define _(String)		gettext(String)
#define RTB_LOCALEDIR	"" //TODO (read the gettext documentation...again!
//-----------------END MACROS-------------------------------------------

using namespace std;

//-----------------INI Function prototipes------------------------------
void print_help_message(void);
void parse_command_line(int argc, char **argv);
//-----------------END Function prototipes------------------------------

int main(int argc, char* argv[])
{
	setlocale(LC_ALL,"");
	bindtextdomain(GETTEXT_DOMAIN, RTB_LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_DOMAIN, "UTF-8");
	textdomain(GETTEXT_DOMAIN);
	
	parse_command_line(argc, argv);
	
	return 0;
}

//-----------------INI Function declarations----------------------------

void print_help_message(void)
{
	cout << endl;
	cout << _("Usage: RealTimeBattle [options] ") << endl << endl;
	cout << _("Options:") << endl;
	cout << _("	--debug_level [0-5],	-D	sets the initial debug level.") << endl;
	cout << _("	--option_file [file],	-o	selects option-file (default: $HOME/.rtbrc)")  << endl;
	cout << _("	--log_file [file],	-l	make log file (default: STDOUT), if 'file' is '-'\n"
		"					the log is sent to STDOUT") << endl;
	cout << _("	--help,			-h	prints this message") << endl;
	cout << _("	--version,		-v	prints the version number") << endl;
	cout << endl;
}

void parse_command_line(int argc, char **argv)
{
	int opt = 0;
	
	static struct option long_options[] =
	{
		//option, argument? (0 - none, 1 - mandatory, 2 -optional), flag, value
		{"debug_level",	2,	0,	0},
		{"option_file",	2,	0,	0},
		{"log_file",	2,	0,	0},
		{"help",		0,	0,	'h'},
		{"version",		0,	0,	'v'},
		{0,				0,	0,	0}
	};
	
	int long_index =0;
	while ((opt = getopt_long(argc, argv,"Do:l:hv", long_options, &long_index )) != -1)
	{
		switch (opt)
		{
			case 'h' :
				print_help_message();
				break;
			case 'v':
				cout << endl;
				cout << VERSION;
				cout << endl;
				break;
			default:
				break;
		}
	}
}

//-----------------END Function declarations----------------------------