# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-06-11 02:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/main.cpp:37
msgid "Usage: RealTimeBattle [options] "
msgstr ""

#: src/main.cpp:38
msgid "Options:"
msgstr ""

#: src/main.cpp:39
msgid "\t--debug_level [0-5],\t-D\tsets the initial debug level. Implies -d"
msgstr ""

#: src/main.cpp:40
msgid ""
"\t--option_file [file],\t-o\tselects option-file (default: $HOME/.rtbrc)"
msgstr ""

#: src/main.cpp:41
msgid ""
"\t--log_file [file],\t-l\tmake log file (default: STDOUT), if 'file' is '-'\n"
"\t\t\t\t\tthe log is sent to STDOUT"
msgstr ""

#: src/main.cpp:43
msgid "\t--help,\t\t\t-h\tprints this message"
msgstr ""

#: src/main.cpp:44
msgid "\t--version,\t\t-v\tprints the version number"
msgstr ""
